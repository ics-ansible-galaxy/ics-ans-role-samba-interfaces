import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_samba_interfaces(host):
    file = host.file("/etc/samba/interfaces.conf")
    assert file.contains("interfaces = eth0")

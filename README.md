ics-ans-role-samba-interfaces
=============================

Ansible role to configure samba to bind only on the main interface.
This is to avoid the host broadcasting to AD all the interfaces created by the docker daemon.

To be used on IT machines only.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

None

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-samba-interfaces
```

License
-------

BSD 2-clause
